from Statement import *
from copy import deepcopy

# Single Pass
def reduce_if(statement):
    if isinstance(statement, Statement):
        if statement.connective is not None:
            for i in range(len(statement.subs)):
                statement.subs[i] = reduce_if(statement.subs[i])

            if statement.connective.name == "if": # there are only two sub statements
                new_if_statement = Statement()
                new_if_statement.connective = Connective("or")

                #(A ^ B)
                sub_statement_1 = Statement()
                sub_statement_1.connective = Connective("and")
                sub_statement_1.subs.extend([statement.subs[0]])
                sub_statement_1.subs.extend([statement.subs[1]])

                # not A
                sub_statement_2 = Statement()
                sub_statement_2.connective = Connective("not")
                sub_statement_2.subs.extend([statement.subs[0]])

                new_if_statement.subs.extend([sub_statement_1])
                new_if_statement.subs.extend([sub_statement_2])
                statement = new_if_statement
                #print(f"IF REDUCTION TO: {statement}")

    return statement

def reduce_iff(statement):
    if isinstance(statement, Statement):
        if statement.connective is not None:
            for i in range(len(statement.subs)):
                statement.subs[i] = reduce_iff(statement.subs[i])

            if statement.connective.name == "iff": # there are only two sub statements
                new_iff_statement = Statement()
                new_iff_statement.connective = Connective("or")

                # (A^B)
                sub_statement_1 = Statement()
                sub_statement_1.connective = Connective("and")
                sub_statement_1.subs.extend([statement.subs[0]])
                sub_statement_1.subs.extend([statement.subs[1]])

                # (not A ^ not B)
                sub_statement_2 = Statement()
                sub_statement_2.connective = Connective("and")

                # not A
                new_sub_sub_statement = Statement()
                new_sub_sub_statement.connective = Connective("not")
                new_sub_sub_statement.subs.extend([statement.subs[0]])
                
                #not b
                new_sub_sub_statement2 = Statement()
                new_sub_sub_statement2.connective = Connective("not")
                new_sub_sub_statement2.subs.extend([statement.subs[1]])

                sub_statement_2.subs.extend([new_sub_sub_statement])
                sub_statement_2.subs.extend([new_sub_sub_statement2])

                new_iff_statement.subs.extend([sub_statement_1])
                new_iff_statement.subs.extend([sub_statement_2])

                statement = new_iff_statement
                #print(f"IFF REDUCTION: {statement}")

    return statement

def reduce_xor(statement):
    # expand that mofo
    if isinstance(statement, Statement):
        if statement.connective is not None:
            # Reduce substatements first
            for i in range(len(statement.subs)):
                statement.subs[i] = reduce_xor(statement.subs[i])

            if statement.connective.name == "xor":
                if len(statement.subs) == 0:
                    return False
                # Expand
                new_statement = Statement()
                new_statement.connective = Connective("or")

                for i in range(len(statement.subs)):
                    new_sub_statement = Statement()
                    new_sub_statement.connective = Connective("and")

                    for j in range(len(statement.subs)):
                        if j == i:
                            new_sub_statement.subs.extend([statement.subs[j]])
                        else:
                            new_sub_sub_statement = Statement()
                            new_sub_sub_statement.connective = Connective("not")
                            new_sub_sub_statement.subs.extend([statement.subs[j]]) 
                            new_sub_statement.subs.extend([new_sub_sub_statement])

                    new_statement.subs.extend([new_sub_statement])
                #once expanded
                statement = new_statement
                #print(f"XOR REDUCED TO: {statement}")
    
    return statement

# Done Multiple Times
def reduce_and(statement):
    if isinstance(statement, Statement):
        if statement.connective is not None:
            for i in range(len(statement.subs)):
                statement.subs[i] = reduce_and(statement.subs[i])

            if statement.connective.name == "and":
                is_true = True
                if len(statement.subs) == 0:
                    return True

                if False in statement.subs:
                    return False

                for x in statement.subs:
                    if x is not True:
                        is_true = False
                        break
                if is_true:
                    return True
                # TODO: t and not t == false
                # Compare the strings Luke!
                strings = []
                for x in statement.subs:
                    strings.extend([str(x)])

                for x in strings:
                    if x[1:3] == "not":
                        pass
                    else:
                        new_string = "(not " + str(x) + ")"

                        if new_string in strings:
                            return False

    return statement

def reduce_or(statement):
    if isinstance(statement, Statement):
        if statement.connective is not None:
            for i in range(len(statement.subs)):
                statement.subs[i] = reduce_or(statement.subs[i])

            if statement.connective.name == "or":
                if True in statement.subs:
                    return True
                
                if len(statement.subs) == 0:
                    return False

                is_false = True
                for i in statement.subs:
                    if  i is False:
                        pass
                    else:
                        is_false = False
                
                if is_false:
                    return False

                strings = []
                for x in statement.subs:
                    strings.extend([str(x)])

                # T or not T
                for x in strings:
                    if x[1:3] == "not":
                        pass
                    else:
                        new_string = "(not " + str(x) + ")"
                        
                        if new_string in strings:
                            return True
                    
    return statement
    
def reduce_not(statement):
    if isinstance(statement, Statement):
        if statement.connective is not None:

            for i in range(len(statement.subs)):
                statement.subs[i] = reduce_not(statement.subs[i])
            
            if statement.connective.name == "not":
                if statement.subs != []:
                    if statement.subs[0] is True:
                        return False

                    elif statement.subs[0] is False:
                        return True

                if len(statement.subs) == 1 and isinstance(statement.subs[0], Symbol) and statement.connective is None:
                    statement.symbol = statement.subs[0]
                    statement.subs = []

                if statement.symbol is None and not isinstance(statement.subs[0], Symbol):
                    if statement.subs[0].symbol is None:
                        if statement.subs[0].connective.name == "not":
                            #print(f"PRIOR: {statement}")
                            statement = statement.subs[0].subs[0]
                            statement.conective = None
                            #print(f"NOT REDUCTION: {statement}")

    return statement

# Main Normalization function
def normalize(statements):
    normalized = []
    for statement in statements:
        print(f"Starting at: {statement}")
        statement = reduce_if(statement)
        statement = reduce_iff(statement)
        statement = reduce_xor(statement)
        statement = reduce(statement)
        print(f"Normalized To: {statement}")
        normalized.extend([statement])
    return normalized

# Repeates reductions until there is no change
def reduce(statement):
    loop = 0
    while True:
        old_copy = deepcopy(statement)
        statement = reduce_and(statement)
        statement = reduce_or(statement)
        statement = reduce_not(statement)

        # if there are no changes 
        if str(old_copy) == str(statement):
            break

        loop += 1
    #print(f"Reduction loops: {loop}")
    return statement