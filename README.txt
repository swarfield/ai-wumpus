Samuel Warfield: 10731212
Python 3.7

How the code is structured:
The code is seporated into 5 distinct parts:
    Main:       Launches the parser, reducer, DPLL SAT solver, then runs the inference engine, then returns the results
    Parser:     Tokenizes and then parses the txt files into usable data structured
    Reducer:    Normalizes the statements into only having symbols (and, not, or)
    DPLL:       Solves for what cases the knowledge base is satifiable
    Entailment: Given all satifiable instances of the knowledge base is the statement Entailed

How to run the code, including very specific compilation instructions, if compilation is needed. Instructions such as "compile using g++" are NOT considered specific.
    $ python3 main.py wumpus_rules.txt [additional_knowledge_file] [statement_file]