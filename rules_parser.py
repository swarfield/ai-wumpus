import re
from Statement import Statement, Symbol, Connective

# Returns a list of tokenized statements
def tokenize(filename):
    # Use a regex to tokenize file
    tokenre = re.compile(r'([^\s\(\)]+|[\(\)])')
    statements = []
    print(filename)

    try:
        with open(filename,'r') as f:
            for line in f:
                if line == "\n":
                    continue

                if "#" in line:
                    line = line[0:line.find("#")]

                thing = tokenre.finditer(line)
                tokens = []

                for x in list(thing):
                    tokens.extend([x.group(0)])

                if tokens != []:
                    statements.append(tokens)

        return statements

    except FileNotFoundError:
        print(filename + ' File does not exist')
        exit(-1)



def parse(filename):
    statement_strings = tokenize(filename)
    statements = []
    conectives = ("and", "or", "xor", "not", "if", "iff")

    # Get tokens to Statments
    for tokens in statement_strings:
        statement_tokens = []
        # Conver raw tokens to correct base types (Symbols and Connectives)
        for token in tokens:
            if token.lower() in conectives:
                statement_tokens.extend([Connective(token.lower())])
                continue
            elif token == "(" or token == ")":
                statement_tokens.extend([token])
            else:
                statement_tokens.extend([Symbol(token)])

        # Shift Reduce

        if isinstance(statement_tokens[0], Symbol):
            new_statment = Statement()
            new_statment.symbol = statement_tokens[0]
            statement_tokens[0] = new_statment

        the_stack = []

        while len(statement_tokens) > 0:

            current = statement_tokens[0]
            statement_tokens = statement_tokens[1:]

            if current == ")":
                a_new_statement = Statement()
                while True:
                    x = the_stack.pop()
                    if x == "(":
                        break
                    if isinstance(x, Connective):
                        a_new_statement.connective = x
                    else:
                        a_new_statement.subs.extend([x])

                a_new_statement.subs = a_new_statement.subs[::-1]
                the_stack.extend([a_new_statement])

            else:
                the_stack.extend([current])

        #print(f"Final Statement: {the_stack[0]}")
        statements.extend([the_stack[0]])

    for statement in statements:
        finalParse(statement)    
    return statements

def finalParse(statement):
    if len(statement.subs) == 1:
        if isinstance(statement.subs[0], Symbol):
            if statement.connective is not None:
                new_sub = Statement()
                new_sub.symbol = statement.subs[0]
                statement.subs[0] = new_sub
            else:
                statement.symbol = statement.subs[0]
                statement.subs = []
        else:
            finalParse(statement.subs[0])
    else:
        for x in statement.subs:
            if isinstance(x, Statement):
                finalParse(x)

    return statement