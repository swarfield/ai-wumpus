from Statement import *

# Impliments the DPLL Algorithm for Satifaction
# https://en.wikipedia.org/wiki/DPLL_algorithm

"""
Algorithm DPLL
  Input: A set of clauses Φ.
  Output: A Truth Value.

function DPLL(Φ)
   if Φ is a consistent set of literals
       then return true;
   if Φ contains an empty clause
       then return false;
   for every unit clause {l} in Φ
      Φ ← unit-propagate(l, Φ);
   for every literal l that occurs pure in Φ
      Φ ← pure-literal-assign(l, Φ);
   l ← choose-literal(Φ);
   return DPLL(Φ ∧ {l}) or DPLL(Φ ∧ {not(l)});
"""

# This will find the Satisfiable cases for the KB / Rule set
def dpll(kb):
    # Get list of all symbols in knowledge base
    syms = []
    for x in kb:
        syms.extend(get_symbols(x))
        
    syms = set(syms)
    syms = list(syms)

    for z in syms:
        print(z)

    

    return []

def actual_dpll(hi, bar):
    pass

def get_symbols(statement):
    symbols = []

    if isinstance(statement, bool):
        return []

    if statement.symbol is None:
        for x in statement.subs:
            if isinstance(x, Symbol):
                if x in symbols:
                    pass
                else:
                    symbols.extend([x])
                    continue

            newSyms = get_symbols(x) 

            for y in newSyms:
                if y not in symbols:
                    symbols.extend([y])
        
        return symbols
    else:
        return [statement.symbol]
    return symbols