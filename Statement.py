class Statement(str):
    def __init__(self):
        self.connective = None
        self.symbol = None
        self.subs = []

    def __str__(self):
        if self.symbol is not None:
            return(str(self.symbol))

        elif self.connective is not None:
            returner = f"({self.connective} "
            for x in self.subs:
                returner += str(x)
                returner += ' '
            returner = returner[0:-1]
            returner += ')'
            return returner

class Symbol(str):
    def __init__(self, name):
        self.name = name
        self.value = None
    
    def __str__(self):
        return self.name
    
class Connective(str):
    def __init__(self, name):
        self.name = name
    def __str__(self):
        return self.name