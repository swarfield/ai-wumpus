# #!/usr/bin/python3
# Author: Samuel Warfield

import sys
import rules_parser
import Statement
from inference import infer
from reducer import normalize
from dpll import dpll

def main(argv):
    if len(argv) != 3:
        print("Usage: python3 main.py wumpus_rules.txt [additional_knowledge_file] [statement_file]")
        exit(-1)

    rulesfile, add_file, statement_file = argv

    print("Parsing files")
    # Parse Rules
    wumpus_rules = rules_parser.parse(rulesfile)

    # Parse KB
    wumpus_rules.extend(rules_parser.parse(add_file))

    # Parse Statment
    statement = rules_parser.parse(statement_file)

    print("\nNormalizing logical statements\n")
    # Normalize (To statements that only contain AND, OR, and NOT)
    wumpus_rules = normalize(wumpus_rules)
    statement = normalize(statement)

    # Clean out the Tautologies from the kb
    for x in wumpus_rules:
        print(x)
        if x is False:
            print("KB never satisfiable")
            print("possibly true or false")
            exit(1)
        if x is True:
            wumpus_rules.remove(x)

    # DPLL
    print("\nApplying DPLL SAT Solver\n")
    satisfiable = dpll(wumpus_rules)

    if len(satisfiable) == 0:
        print("KB never satisfiable")
        print("possibly true or false")
    
    # Inference
    not_statement = Statement.Statement()
    not_statement.connective = Statement.Connective("not")
    not_statement.subs = [statement[0]]

    statement = infer(statement, satisfiable)
    not_statement = infer(not_statement, satisfiable)

    if statement and not_statement:
        print("Both True and False")
    elif statement and not not_statement:
        print("Definitely True")
    elif  not statement and not_statement:
        print("Definitely False")
    else:
        print("Possible True or False")

    print("\nDefinitely not done")

if __name__ == "__main__":
    main(sys.argv[1:])